import React from "react";
import { gql, useQuery } from "@apollo/client";
import LaunchItem from "./LaunchItem";
import MissionLegends from "./MissionLegends";

const LAUNCHES_QUERY = gql`
  query LaunchesQuery {
    launches {
      flight_number
      mission_name
      launch_date_local
      launch_success
    }
  }
`;

const Launches = () => {
  const { loading, error, data } = useQuery(LAUNCHES_QUERY);

  if (loading) return <h4>Loading...</h4>;
  if (error) return `Error! ${error}`;

  return (
    <div>
      <h1 className="display-4 my-3">Launches</h1>
      <MissionLegends />
      {data.launches.map((launch, index) => (
        <LaunchItem key={index} launch={launch} />
      ))}
    </div>
  );
};

export default Launches;
